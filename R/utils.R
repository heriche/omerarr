#' get_s3_client
#'
#' Wrapper around paws.storage::s3()
#'
#' @importFrom paws.storage s3
#' @param endPoint URL of the service host
#' @param accessKey access key
#' @param secretKey secret key
#' @param region AWS region (default: empty string)
#' @returns a list representing paws.storage s3 client
#' @export

get_s3_client <- function(endPoint, accessKey = NULL, secretKey = NULL, region = "") {
  credentials <- list()
  if(is.null(accessKey)) { # Assume credentials are provided in environment variables
     if(Sys.getenv("AWS_S3_ENDPOINT") != "" && Sys.getenv("AWS_ACCESS_KEY_ID") != "") {
       credentials$creds <- list(access_key_id = Sys.getenv("AWS_ACCESS_KEY_ID"),
                                 secret_access_key  = Sys.getenv("AWS_SECRET_ACCESS_KEY_ID"))
       endPoint <- Sys.getenv("AWS_S3_ENDPOINT")
       region <- Sys.getenv("AWS_DEFAULT_REGION")
     } else {
       credentials$anonymous <- TRUE
     }
  } else {
    credentials$creds <- list(access_key_id = accessKey, secret_access_key = secretKey)
  }
  s3.client <- paws.storage::s3(
    config = list(
      credentials = credentials,
      endpoint = endPoint,
      region = region
    )
  )
  return(s3.client)
}

#' Functions below copied/modified from Rarr S3.R
#' to avoid using :::
#'
#' @param path URL of an ome-zarr container
#'

parse_s3_url <- function(path) {

  provider <- NULL
  if (!grepl(pattern = "(^https?://)|(^s3://)", x = path)) {
    provider <- NULL
  } else {
    matches <- regmatches(
      x = path,
      m = regexpr(
        pattern = "(amazonaws\\.com)|(embl\\.de)",
        text = path
      )
    )
    if (!length(matches)) {
      matches <- "other"
    }
    provider <- switch(matches,
                       "amazonaws.com" = "aws",
                       "embl.de"       = "other",
                       "other"
    )
  }
  if(is.null(provider)) {
    parsed_url <- NULL
  } else if(provider == "aws") {
    parsed_url <- .parse_aws_url(path)
  } else {
    parsed_url <- .parse_other_url(path)
  }
  return(parsed_url)
}

#' @importFrom httr parse_url
#' @keywords Internal
.parse_aws_url <- function(url) {
  tmp <- parse_url(url)

  if (grepl(pattern = "^https?://s3\\.", x = url, ignore.case = TRUE)) {
    ## path style address
    bucket <- gsub(x = tmp$path, pattern = "^/?([a-z0-9\\.-]*)/.*",
                   replacement = "\\1", ignore.case = TRUE)
    object <- gsub(x = tmp$path, pattern = "^/?([a-z0-9\\.-]*)/(.*)",
                   replacement = "\\2", ignore.case = TRUE)
    region <- gsub(x = url,
                   pattern = "^https?://s3\\.([a-z0-9-]*)\\.amazonaws\\.com/.*$",
                   replacement = "\\1")
  } else if (grepl(pattern = "^https?://[a-z0-9\\.-]*.s3\\.", x = url, ignore.case = TRUE)) {
    ## virtual-host style address
    bucket <- gsub(x = tmp$hostname, pattern = "^([a-z0-9\\.-]*)\\.s3.*",
                   replacement = "\\1", ignore.case = TRUE)
    object <- tmp$path
    region <- gsub(x = tmp$hostname,
                   pattern = "^.*\\.s3\\.([a-z0-9-]*)\\.amazonaws\\.com$",
                   replacement = "\\1", ignore.case = TRUE)
  } else {
    stop("Unknown AWS path style.  Please report this to the package maintainer.")
  }

  res <- list(bucket = bucket, object = object, region = region,
              hostname = "https://s3.amazonaws.com")
  return(res)
}

#' @importFrom httr parse_url
#' @keywords Internal
.parse_other_url <- function(url) {
  parsed_url <- httr::parse_url(url)
  bucket <- gsub(x = parsed_url$path, pattern = "^/?([[a-z0-9\\.-]*)/.*",
                 replacement = "\\1", ignore.case = TRUE)
  object <- gsub(x = parsed_url$path, pattern = "^/?([a-z0-9\\.-]*)/(.*)",
                 replacement = "\\2", ignore.case = TRUE)

  res <- list(bucket = bucket,
              object = object,
              region = "auto",
              hostname = paste0(parsed_url$scheme, "://", parsed_url$hostname))
  return(res)
}

#' Function to set a list element to NULL because assigning NULL to a list element destroys it
#' Useful because Rarr::read_zarr_array index argument is a list with NULL elements
#' @export
set_to_null <- function(L, idx) {
  seq.along <- seq_along(L)
  vec.subset <- seq.along %in% idx
  res <- ifelse(vec.subset, lapply(seq.along, function(x) NULL), L)
  attrs.new <- attributes(res)
  attributes(res) <- if(!is.null(attrs.new))
    modifyList(attributes(L), attrs.new) else attributes(L)
  return(res)
}

